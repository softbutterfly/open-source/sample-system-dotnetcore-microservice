using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SaludoApp.Controllers
{
    [ApiController]
    public class SaludoController : ControllerBase
    {
        [HttpGet("/")]
        public ActionResult<string> Saludar(string name = "Mundo")
        {
            return "¡Hola," + name + " saludos de NET!";
        }
    }
}
FROM mcr.microsoft.com/dotnet/sdk:7.0

ENV ASPNETCORE_URLS=http://+:5000/

COPY . /app

RUN apt-get update && apt-get install -y wget ca-certificates gnupg \
&& echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | tee /etc/apt/sources.list.d/newrelic.list \
&& wget https://download.newrelic.com/548C16BF.gpg \
&& apt-key add 548C16BF.gpg \
&& apt-get update \
&& apt-get install -y 'newrelic-dotnet-agent' \
&& rm -rf /var/lib/apt/lists/*

ENV CORECLR_ENABLE_PROFILING=1 \
CORECLR_PROFILER={36032161-FFC0-4B61-B559-F6C5D41BAE5A} \
CORECLR_NEWRELIC_HOME=/usr/local/newrelic-dotnet-agent \
CORECLR_PROFILER_PATH=/usr/local/newrelic-dotnet-agent/libNewRelicProfiler.so \
NEW_RELIC_LICENSE_KEY=${NEWRELIC_LICENSE_KEY:-samplelicensekey} \
NEW_RELIC_APP_NAME=${NEWRELIC_APP_NAME:-sampleappname}

WORKDIR /app

RUN dotnet publish -c Release --output bin/Release/net7.0


ENTRYPOINT ["dotnet","bin/Release/net7.0/greeting.dll"]


